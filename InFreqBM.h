/* 
 * File:   InFreqBM.h
 * Author: aeon
 *
 * Created on 24 January, 2013, 8:08 PM
 */

#ifndef INFREQBM_H
#define	INFREQBM_H
#include "subsetGen.h"
#include <set>

using namespace std;

class InFreqBM 
{
private:
    bool *bitMap;    
    int countItems;
    int countMaxInFreq;    
    int getIndex(set<int> &);
    
public:
    InFreqBM(int);    
    void add(set<int> &);
    bool subsetCheck(set<int> &);
    virtual ~InFreqBM();
    void stub(set<int> &s);
    long int cardinality(void);
};

#endif	/* INFREQBM_H */

