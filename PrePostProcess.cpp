/* 
 * File:   PreProcess.cpp
 * Author: aeon
 * 
 * Created on 10 April, 2013, 10:01 PM
 */

#include "PrePostProcess.h"
#include <cstring>
#include <string>
#include <map>
#include <sstream>
#include <cstdlib>
#include <istream>
#include <fstream>
#include <iostream>

#define buffer_size 1024*1024*1024
#define line_size 2*1024
#define max_item_length 100


PrePostProcess::PrePostProcess(const char * filepath,const char delim) {
    int l=strlen(filepath);
    fname=NULL;
    ofname=NULL;
    mapfname=NULL;            
    fname=new char[l+1];
    ofname= new char[l+6];
    mapfname= new char[l+10];
    strcpy(fname,filepath);    
    int i;
    for (i=0;i<l;i++)
    {
        if (fname[i]!='.')
        {
            ofname[i]=fname[i];
            mapfname[i]=fname[i];
        }
        else
            break;
    }
    ofname[i+1]='\0';
    mapfname[i+1]='\0';
    strcat(ofname,"_int.txt");
    strcat(mapfname,"_int_map.txt");
    delimiter=delim;
    
}
char* PrePostProcess::preProcess()
{
    map<string,int> itemToId;
    istringstream strstream;
    ifstream ds_file;    
    ofstream outfile(ofname,ios::binary);
    ofstream mapfile(mapfname,ios::binary);
    int runningId=0;
    int len=0;
    char * startPos;    
    ds_file.open(fname, std::ifstream::in);
    if (ds_file.is_open())
    {
        char *buf = new char[buffer_size];
        char *line = new char[line_size];        
        while (!(ds_file.eof()))
        {
            ds_file.read(buf,buffer_size);                                                                            
            strstream.rdbuf()->pubsetbuf(buf,buffer_size);                 
            while (strstream.good())
            {                             
                strstream.getline(line,line_size,'\n');  
                int l_line = strlen(line);
                if(l_line>1)
                {                    
                    if (line[l_line-1]!=delimiter)
                    {                        
                        line[l_line]=delimiter;
                        line[l_line+1]='\0';
                    }    
                    len=0;
                    startPos=line;
                    for (int i=0;i<strlen(line);i++)
                    {
                        if (line[i]==delimiter)
                        {            
                            string item (startPos,len);

                            if (itemToId.find(item)==itemToId.end())
                            {
                                itemToId[item]=runningId;                            
                                runningId++;
                            }
                            outfile<<itemToId[item]<<delimiter;
                            len=0;
                            startPos=line+1+i;                        
                        }
                        else           
                        {
                            len++;                        
                        }
                    }     
                    outfile<<"\n";
                }
           }
        }                
        delete[] buf;
        delete[] line;
        ds_file.close();
        map<string,int>::iterator it;        
        for (it=itemToId.begin();it!=itemToId.end();it++)
        {            
            mapfile<<it->first<<delimiter<<it->second<<"\n";            
        }
    }
    else
    {
        cout<<"\nCould not open file\n";
        exit(1);
    }
    outfile.close();    
    return ofname;
}

char* PrePostProcess::postProcess(const char* rfname)
{
    map<int,string> idToItem;
    int i;    
    int l=strlen(rfname);      
    resfname = new char[l+10];
    strcpy(resfname,rfname);    
    for (i=l;i>=0;i--)
    {
        if (resfname[i]=='.')
            break;
    }
    resfname[i]='\0';
    strcat(resfname,"_verbrose.txt");
    istringstream strstream;
    ifstream ds_file;
    int len=0;
    char * startPos; 
    //reading map file
    ds_file.open(mapfname, std::ifstream::in);
    if (ds_file.is_open())
    {
        char *buf = new char[buffer_size];
        char *line = new char[line_size];        
        while (!(ds_file.eof()))
        {
            ds_file.read(buf,buffer_size);                                                                            
            strstream.rdbuf()->pubsetbuf(buf,buffer_size);                 
            while (strstream.good())
            {                             
                strstream.getline(line,line_size,'\n');                    
                len=0;
                startPos=line;
                for (l=0;l<strlen(line);l++)
                {
                    if (line[l]==delimiter)
                        break;                        
                }
                string item(startPos,l);                
                int sum=0;
                for (l=l+1;l<strlen(line);l++)
                {
                    sum = (sum*10)+(line[l]-'0');
                }
                if (idToItem.find(sum)==idToItem.end())
                { 
                        idToItem[sum]=item;                
                }
           }
        }           
        delete[] buf;
        delete[] line;
        ds_file.close();       
    }
    
    //reading and converting result file
    ds_file.open(rfname, std::ifstream::in);
    ofstream outfile(resfname,ios::binary);
    istringstream strstream2;
    if (ds_file.is_open())
    {
        
        char *buf = new char[buffer_size];
        char *line = new char[line_size];        
        while (!(ds_file.eof()))
        {

            ds_file.read(buf,buffer_size);                                                                            
            strstream2.rdbuf()->pubsetbuf(buf,buffer_size);                             
            while (strstream2.good())
            {                  
                strstream2.getline(line,line_size,'\n');  //extracting line of data 
                //below processing is done per line of file
                if (strlen(line)>8)
                {
                    int sum=0;

                    for(i=0;line[i]!='{';i++);
                    outfile<<"{";
                    i++;
                    while(line[i]!='}')
                    {
                        if (line[i]==',')
                        {
                            outfile<<idToItem[sum]<<", ";
                            sum=0;
                        }
                        else
                        {
                            if (isdigit(line[i]))
                                    sum = (sum*10)+(line[i]-'0');
                        }

                        i++;
                    }
                    outfile<<idToItem[sum]<<", ";
                    sum=0;
                    outfile.seekp(-2,ios::cur);
                    outfile<<"} exclusive {";
                    for(;line[i]!='{';i++);                
                    i++;                
                    while(line[i]!='}')
                    {
                        if (line[i]==',')
                        {
                            outfile<<idToItem[sum]<<", ";                        
                            sum=0;
                        }
                        else
                        {
                            if (isdigit(line[i]))
                                    sum = (sum*10)+(line[i]-'0');
                        }

                        i++;
                    }   
                    outfile<<idToItem[sum]<<", ";                
                    sum=0;
                    outfile.seekp(-2,ios::cur);
                    outfile<<"}";
                    i++;
                    while(i<strlen(line))
                    {
                            outfile<<line[i];
                            i++;
                    }
                    outfile<<"\n";
                }
           }
        }                
        delete[] buf;
        delete[] line;
        ds_file.close();
        outfile.close();
    }
    return resfname;
}
PrePostProcess::~PrePostProcess() {
    if(mapfname!=NULL)
    {    
        delete [] mapfname;
    }
    if (ofname!=NULL)
    {
        delete [] ofname;
    }
    
    if(fname!=NULL)
    {
        delete [] fname;
    }
    
}
char * PrePostProcess::getMapFile(void)
{
    return mapfname;
}

