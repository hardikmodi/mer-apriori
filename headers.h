/* 
 * File:   Dataset.h
 * Author: aeon
 *
 * Created on 19 December, 2012, 9:28 PM
 */

#include <string>
#include <set>

using namespace std;

#ifndef DATASET_H
#define	DATASET_H


class Dataset
{
    friend class MERMiner;
    private:
        char *path;
        char delimiter;
        set<int> items;
        long int no_of_items;
        long int no_of_transactions;
        bool ** ibm;
        void parseDataset(void);
        void createIBM(void);
        
    public:        
        Dataset( const char *ds_file_path,char delim);
        void stub(void);
        ~Dataset();
        
};
#endif	/* DATASET_H */

/* 
 * File:   SetOfSet.h
 * Author: aeon
 *
 * Created on 19 December, 2012, 10:03 PM
 */

#include <set>

using namespace std;

#ifndef SETOFSET_H
#define	SETOFSET_H

class SetOfSet
{
    private:
        set< set<int> > s;
    public:
        SetOfSet(){};        
        virtual ~SetOfSet(){};              
        bool subsetCheck(set<int> &x);
        bool add(set<int>);
        bool isIntersection(const set<int>& a, const set<int>& b);
        long int cardinality(void);
        set< set<int> >::iterator first(void); 
        set< set<int> >::iterator last(void); 
        void stub(void);
};
#endif	/* SETOFSET_H */

/* 
 * File:   MERMiner.h
 * Author: aeon
 *
 * Created on 19 December, 2012, 9:33 PM
 */

#include "SetOfSet.h"
#include <string>
#include <map>
#include "Dataset.h"

using namespace std;

#ifndef MERMINER_H
#define	MERMINER_H


class MERMiner
{
    private:
        Dataset &ds;
        SetOfSet MaxInFreq;
        SetOfSet Freq;
        float minsup;
        float minconf;
        long int minsupportcount;
        string output_file_path;            
        set<int> candidate,X,Y;
        map<set<int> , float> FreqSupp;
        void GetNextCandidateItemset(void);
        float GetSupportCount(void);
        void GetNextRule(void);
        float CalculateConfidence(void);
        long int fwrite_rule(ofstream& out, set<int> a,set<int> b);
    public:
        MERMiner(Dataset &d);
        void mine(char  *o_path,float m_sup,float m_conf);

};
#endif	/* MERMINER_H */

