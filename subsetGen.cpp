/* 
 * File:   subsetGen.cpp
 * Author: aeon
 * 
 * Created on 25 January, 2013, 11:29 PM
 */

#include "subsetGen.h"
#include <set>
#include <iostream>
using namespace std;


subsetGen::subsetGen(set<int> &s) {
    buf.clear();
    n=s.size();
    ele=NULL;
    ele = new int[n];    
    int idx=0;
    for (set<int>::iterator itr=s.begin();itr!=s.end();itr++)
    {    
        ele[idx++]=*itr;        
    }    
    current = n-1;
    last =n; 
    ptrs=NULL;
}


subsetGen::~subsetGen() {    
    if (ele!=NULL)
        delete [] ele;
    ele=NULL;
    ptrs=NULL;
    buf.clear();
}

void subsetGen::getNextSubset(void)
{
    if(current>0)
	{
		if(current < last)
		{
                        buf.clear();
			if (ptrs!=NULL)
				delete [] ptrs;			
			ptrs = new int[current];
			for (int idx=0;idx<current;idx++)
			{
				ptrs[idx] = idx;
				buf.insert(ele[ptrs[idx]]);
			}
			last=current;
		}
		else
		{
			if ((ptrs[current-1])!=n-1)
			{
                           
				buf.erase(ele[ptrs[current-1]]);
				ptrs[current-1]++;
				buf.insert(ele[ptrs[current-1]]);
			}
			else
			{
                            
				int p=current-1;
				while((p>0) && (((ptrs[p])-1) == (ptrs[p-1])))
					p--;
				if (p==0)
				{                                    
					current--;                                        
					getNextSubset();					
				}
				else
				{
                                    
					buf.erase(ele[ptrs[p-1]]);
					ptrs[p-1]++;
					buf.insert(ele[ptrs[p-1]]);
					while(p<current)
					{
						buf.erase(ele[ptrs[p]]);
						ptrs[p]=ptrs[p-1]+1;
						buf.insert(ele[ptrs[p]]);
						p++;
					}
				}
			}
		}
	}
	else
	{            
            if (ptrs!=NULL)
                delete [] ptrs;
            buf.clear();
	}
}
