/* 
 * File:   MERMiner.h
 * Author: aeon
 *
 * Created on 19 December, 2012, 9:33 PM
 */

#include "SetOfSet.h"
#include <string>
#include <map>
#include "Dataset.h"
#include "InFreqBM.h"
using namespace std;

#ifndef MERMINER_H
#define	MERMINER_H


class MERMiner
{
    private:
        Dataset &ds;        
        SetOfSet Freq;
        float minsup;
        float minconf;
        long int minsupportcount;
        string output_file_path;            
        set<int> candidate,X,Y;
        map<set<int> , float> FreqSupp;
        void getNextCandidateItemset(void);
        float getSupportCount(void);
        void getNextRule(void);
        float calculateConfidence(void);
        long int writeRuleToFile(ofstream& out, set<int> a,set<int> b);
        long int writeRuleToFile(ofstream& out, set<int> a,set<int> b,float confval);
        set<int> workingSet;
        void updateWorkingSet(void);
        void showCandidate(void);
    public:
        MERMiner(Dataset &d);
        long int mine(char  *o_path,float m_sup,float m_conf);             
        void showRule(void); 
        clock_t timeGetNextCandidateItemset, timeGetSupportCount, timeGetNextRule, timeCalculateConfidence, timeSubsetCheck,timePhase1;
        long int countGetNextCandidateItemset, countGetSupportCount, countGetNextRule, countCalculateConfidence;
        
        
};






#endif	/* MERMINER_H */

